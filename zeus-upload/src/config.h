#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QJsonObject>

class Config {
public:
    explicit Config();
    bool load(QString filepath);
    void show();

    QString getBrokerAdress();
    int getBrokerPort();
    QString getUploadFolder();
	int getUploadFolderMaxSpace();
	QString getBackupUploadFolder();
	int getBackupFolderMaxSpace();
    QString getUploadUrl();
    int getUploadPeriod();
    int getCompactingSpeed();
private:
    QJsonObject m_json;
}; 

#endif // CONFIG_H
