#
#   Qt modules
#
QT += core
QT += network
QT -= gui

#
#   Project configuration and compiler options
#
CONFIG += c++11
CONFIG += warn_on
CONFIG += console

#
#   Output config
#
TEMPLATE = app
TARGET = zeus-upload
target.path = $$[QT_INSTALL_BINS]
INSTALLS += target




#
#   Sources
#
SOURCES += \
    main.cpp \
    debug.cpp \
    sender.cpp \
    config.cpp \
    zeusbustimeout.cpp\
    fileuploader.cpp

HEADERS += \
    sender.h \
    debug.h \
    config.h \
    zeusbustimeout.h\
    fileuploader.h

RESOURCES += \
    bundle.qrc

