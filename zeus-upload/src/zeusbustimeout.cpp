#include "zeusbustimeout.h"
#include <QDebug>
#include <QCoreApplication>
#include <QThread>

ZeusBusTimeout::ZeusBusTimeout(QUrl url, QObject *parent) :
    QObject(parent)
{
    m_url = url;
    m_pause = new QEventLoop(this);
    m_timer = new QTimer(this);
    m_timer->setSingleShot(true);
    m_timeElapsed = 0;
    connect(m_timer, SIGNAL(timeout()), m_pause, SLOT(quit()));
}

ZeusBusTimeout::ZeusBusTimeout(QObject *parent) :
    QObject(parent)
{
    m_pause = new QEventLoop(this);
    m_timer = new QTimer(this);
    m_timer->setSingleShot(true);
    m_timeElapsed = 0;
    connect(m_timer, SIGNAL(timeout()), m_pause, SLOT(quit()));
}

void ZeusBusTimeout::addExitSignal(QObject *sender, const char *sig)
{
    connect(sender, sig, this, SLOT(onExit()));
}

void ZeusBusTimeout::addContinueSignal(QObject *sender, const char *sig)
{
    connect(sender, sig, this, SLOT(onContinue()));
}

bool ZeusBusTimeout::wait(int msec)
{
    m_rc = true;
    m_timeout = msec;

    // Start the timer
    m_timer->start(m_timeout);

    // Start measure of time
    m_chrono.start();

    // Block in a local event loop
    m_pause->exec();

    // save time Elapsed
    m_timeElapsed = m_chrono.elapsed();

    return m_rc;
}

void ZeusBusTimeout::onExit()
{
    m_rc = false;
    m_timer->stop();
    m_pause->quit();
}

void ZeusBusTimeout::onExit(QUrl url)
{
    if(m_url == url) {
        m_rc = false;
        m_timer->stop();
        m_pause->quit();
    }
}

void ZeusBusTimeout::onContinue()
{
    m_timer->start(m_timeout);
}

void ZeusBusTimeout::onContinue(QUrl url)
{
    if(m_url == url) {
        m_timer->start(m_timeout);
    }
}

int ZeusBusTimeout::timeElapsed() const
{
    return m_timeElapsed;
}


