#include "sender.h"
#define UNUSED(x) (void)(x)

using namespace std;

Sender::~Sender()
{

}

Sender::Sender()
{

}

Sender::Sender(QUrl url)
{
    m_url = url;
}

ReturnEnvoi Sender::sendFile(QString pathFile, QString nameFile)
{
    qStdout() << "sendFile" << Qt::endl;

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    ReturnEnvoi ret;
    QHttpPart imagePart;

    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"file\"; filename=\""+nameFile+"\""));
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, "application/raw");
    QFile *file = new QFile(pathFile+"/"+nameFile);
     if (!file->open(QIODevice::ReadOnly)){
         qStdout() << "error open file" << Qt::endl;
         return ERROR;
     }
    imagePart.setBodyDevice(file);
    file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

    multiPart->append(imagePart);
	m_fileSend = true;
    QUrl url(m_url);
    QNetworkRequest request(url);

    QNetworkReply *reply;

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);


    reply = m_nam.post(request,multiPart);

    qStdout() << "sendFile" << " " << multiPart<< Qt::endl;

    multiPart->setParent(reply);
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(messageErreur(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), this, SLOT(enregistrer()));
    connect(reply, SIGNAL(uploadProgress(qint64, qint64)), this, SLOT(progressionTelechargement(qint64, qint64)));

    ZeusBusTimeout zeusTimeout;
    zeusTimeout.addContinueSignal(reply,SIGNAL(uploadProgress(qint64, qint64)));
    zeusTimeout.addExitSignal(reply, SIGNAL(error(QNetworkReply::NetworkError)));
    zeusTimeout.addExitSignal(reply, SIGNAL(finished()));
    zeusTimeout.addExitSignal(this, SIGNAL(abortSignal()));
    m_timeout = zeusTimeout.wait(15000);
    if (m_timeout) {
        reply->abort();
        qStdout() << "timeout !!!!!" << Qt::endl;
		ret = TIMEOUT;
	}else if(!reply->isFinished() || m_fileSend == false){
        qStdout() << "error !!!!!" << Qt::endl;
        ret = ERROR;
    }else{
      //  qDebug() << "finish !!!!!" << Qt::endl;
        ret = OK;
    }
    return ret;
}
void Sender::enregistrer()
{
     //   qDebug() << "upload end" <<Qt::endl;
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender()); //On récupère la réponse du serveur
    QByteArray reponse = r->readAll();
    QString reponseString = reponse;
    if(!reponseString.startsWith("OK")){
        qStdout() << "error : "<< reponseString << Qt::endl;
        m_fileSend = false;
    }
}

void Sender::abort()
{
    qStdout() << "abort"<<Qt::endl;
    emit abortSignal();
}

void Sender::progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal)
{
    if(bytesReceived != 0 && bytesTotal!=0){
       // qDebug() << bytesReceived << " / "<< bytesTotal<< Qt::endl;
    }
}
void Sender::messageErreur(QNetworkReply::NetworkError)
{
	m_fileSend = false;
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender());
    qStdout() << "error reponse : "<< r->errorString() << Qt::endl;
}

