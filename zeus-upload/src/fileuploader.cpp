#include "fileuploader.h"
#include <QDebug>

FileUploader::FileUploader(Config myConf,QString mac)
{
     qDebug() << "mac "<<mac <<Qt::endl;

    m_addressBroker = myConf.getBrokerAdress();
    m_portBroker = myConf.getBrokerPort();
    m_mac = mac;
    m_folder = myConf.getUploadFolder();
    m_FolderMaxSpace = myConf.getUploadFolderMaxSpace();
    m_BackupFolder = myConf.getBackupUploadFolder();
    m_BackupFolderMaxSpace = myConf.getBackupFolderMaxSpace();
    m_url = myConf.getUploadUrl();
    m_periodUpload = myConf.getUploadPeriod();
}

FileUploader::~FileUploader()
{
    m_running = 0;
    emit quitSignalAll();
}

int FileUploader::start()
{
    timeoutUpload = new QTimer();
    timeoutUpload->setTimerType(Qt::VeryCoarseTimer);
    timeoutUpload->setInterval(m_periodUpload*1000);
    connect(timeoutUpload, SIGNAL(timeout()), this, SLOT(uploadFileTimeout())) ;
    timeoutUpload->start();
   /* while (m_running) {
        sleep(1);
        qStdout() << "sleep 2" << Qt::endl;
    }*/
    return true;
}

void FileUploader::uploadFileTimeout(){
    /*
     *  Open dir
     */
    QString pathDir = m_folder;
    QString pathDirSecond = m_BackupFolder;
    QDir uploadDir(pathDir);
    QDir uploadDirSecond(pathDirSecond);
    QProcess process;
    if(!uploadDir.exists()){
        qDebug() << "Create dir" << Qt::endl;
        uploadDir.mkdir(pathDir);
    }
    if(!uploadDirSecond.exists()){
        qDebug() << "Create dir" << Qt::endl;
        uploadDirSecond.mkdir(pathDirSecond);
    }
    //   qDebug() << "start while state = "<< m_state <<Qt::endl;
        //SEND GZ
        QStringList stringlist = uploadDir.entryList(QDir::Files | QDir::NoDotAndDotDot,QDir::Time | QDir::Reversed);
        /*
             *  gz fichier
             * All file are "gzed" in main folder, so no need to "gz" files in backup folder
             */
        qint64 tailleSrcMain = 0;
        qint64 tailleSrcSecond = 0;
        for (int i = 0; i < stringlist.size(); i++){
            if (fileNoLoraNoSigfoxNoGz(stringlist.at(i))){
                //system(qPrintable(QString("gzip -f -9 "+pathDir+"/\""+stringlist.at(i)+"\"")));
                QStringList arguments;
                arguments << "-f" << "-9" << pathDir+"/"+stringlist.at(i);
                qStdout() << "gzip" << arguments.at(0) << arguments.at(1) << arguments.at(2);
                process.start("/bin/gzip", arguments);
                process.waitForFinished();
                QString output = process.readAll().toStdString().c_str();
                qStdout() << "Output : " << output << "End of output";
                QString err = process.readAllStandardError().toStdString().c_str();
                qStdout() << "Error : " << err << "End of error";
                if(err.contains("No space left on device")){
                    process.close();
                    qStdout() << "Manage no space left !" << Qt::endl;
                    do{
                        tailleSrcMain = 0;
                        stringlist = uploadDir.entryList(QDir::Files | QDir::NoDotAndDotDot,QDir::Time | QDir::Reversed);
                        if(!stringlist.isEmpty()){
                            for (int k = 0; k < stringlist.size(); k++){
                                if(!m_running){
                                    break;
                                }else{
                                    tailleSrcMain += QFileInfo(m_folder + "/" + stringlist[k]).size();
                                }
                            }
                        }
                        //qStdout() << "size = "<< tailleSrc <<Qt::endl;
                        if(tailleSrcMain > m_FolderMaxSpace){
                            qStdout() << "too old"<<Qt::endl;
                            QFile fileToRemove;
                            QDir BackUpDir(m_BackupFolder);
                            if(BackUpDir.exists()){
                                qStdout() << "Copying to backup folder" << Qt::endl;
                                do{
                                   QStringList stringlistBackup = BackUpDir.entryList(QDir::Files | QDir::NoDotAndDotDot,QDir::Time | QDir::Reversed);
                                    if(!stringlistBackup.isEmpty()){
                                        for (int k = 0; k < stringlistBackup.size(); k++){
                                            if(!m_running){
                                                break;
                                            }else{
                                                tailleSrcSecond += QFileInfo(m_BackupFolder + "/" + stringlistBackup[k]).size();
                                            }
                                        }
                                    }
                                    if(tailleSrcSecond > m_BackupFolderMaxSpace){
                                        if(!fileToRemove.remove(m_BackupFolder+"/"+stringlistBackup.at(0))){
                                            qStdout() << "impossible to remove : " << stringlistBackup.at(0)<<Qt::endl;
                                        }
                                    }
                                }while(tailleSrcSecond > m_BackupFolderMaxSpace);
                                if(!fileToRemove.copy(m_folder+"/"+stringlist.at(0),m_BackupFolder+"/"+stringlist.at(0))){
                                    qStdout() << "impossible to copy : " << stringlist.at(0)<<Qt::endl;
                                }
                            }
                            stringlist = uploadDir.entryList(QDir::Files | QDir::NoDotAndDotDot,QDir::Time | QDir::Reversed);
                            if(!fileToRemove.remove(m_folder+"/"+stringlist.at(0))){
                                qStdout() << "impossible to remove : " << stringlist.at(0)<<Qt::endl;
                            }
                        }
                    }while(tailleSrcMain > m_FolderMaxSpace);
                    process.start("/bin/gzip", arguments);
                    process.waitForFinished();
                }
            }
        }
        /*
             *  envoi gz depuis dossier principale
             */
        stringlist = uploadDir.entryList(QStringList()<<"*.gz",QDir::Files | QDir::NoDotAndDotDot,QDir::Time);
        if(!stringlist.isEmpty()){
            if(askNetwork() < 0){
            }else{
            QUrl url(m_url+"/api/std/v1/data/mac="+m_mac+"/");
            Sender sendApp(url);
            connect(this, SIGNAL(quitSignalAll()), &sendApp, SLOT(abort()));
            for (int i = 0; i < stringlist.size(); i++){
                if(!m_running){
                    break;
                }else{
                    if(sendApp.sendFile(m_folder,stringlist.at(i)) == OK){
                        QFile fileToRemove;
                        if(!fileToRemove.remove(m_folder+"/"+stringlist.at(i))){
                            qDebug() << "impossible to remove : " << stringlist.at(i)<<Qt::endl;
                        }
                    }
                }
            }
            QUrl myUrl(m_url);
            //  qDebug() << "will release"<< Qt::endl;
            disconnect(this, SIGNAL(quitSignalAll()), &sendApp, SLOT(abort()));
        }
        }
        /*
             *  envoi gz depuis dossier backup
             */
        if(uploadDirSecond.exists()){
            QStringList stringlistSecond = uploadDirSecond.entryList(QStringList()<<"*.gz",QDir::Files | QDir::NoDotAndDotDot,QDir::Time);
            if(!stringlistSecond.isEmpty()){
                if(askNetwork() < 0){
                }else{
                QUrl url(m_url+"/api/std/v1/data/mac="+m_mac+"/");
                Sender sendApp(url);
                connect(this, SIGNAL(quitSignalAll()), &sendApp, SLOT(abort()));
                for (int i = 0; i < stringlistSecond.size(); i++){
                    if(!m_running){
                        break;
                    }else{
                        if(sendApp.sendFile(m_BackupFolder,stringlistSecond.at(i)) == OK){
                            QFile fileToRemove;
                            if(!fileToRemove.remove(m_BackupFolder+"/"+stringlistSecond.at(i))){
                                qDebug() << "impossible to remove : " << stringlistSecond.at(i)<<Qt::endl;
                            }
                        }
                    }
                }
                QUrl myUrl(m_url);
                //  qDebug() << "will release"<< Qt::endl;
                disconnect(this, SIGNAL(quitSignalAll()), &sendApp, SLOT(abort()));
            }
        }
        }

        //TOO OLD
        tailleSrcMain = 0;
        tailleSrcSecond = 0;
        do{
            tailleSrcMain = 0;
            stringlist = uploadDir.entryList(QDir::Files | QDir::NoDotAndDotDot,QDir::Time | QDir::Reversed);
            if(!stringlist.isEmpty()){
                for (int i = 0; i < stringlist.size(); i++){
                    if(!m_running){
                        break;
                    }else{
                        tailleSrcMain += QFileInfo(m_folder + "/" + stringlist[i]).size();
                    }
                }
            }
            //qStdout() << "size = "<< tailleSrc <<Qt::endl;
            if(tailleSrcMain > m_FolderMaxSpace){ // size in bytes
                qStdout() << "too old"<<Qt::endl;
                QFile fileToRemove;
                QDir BackUpDir(m_BackupFolder);
                if(BackUpDir.exists()){
                    do{
                        QStringList stringlistBackup = BackUpDir.entryList(QDir::Files | QDir::NoDotAndDotDot,QDir::Time | QDir::Reversed);
                        if(!stringlistBackup.isEmpty()){
                            for (int i = 0; i < stringlistBackup.size(); i++){
                                if(!m_running){
                                    break;
                                }else{
                                    tailleSrcSecond += QFileInfo(m_BackupFolder + "/" + stringlistBackup[i]).size();
                                }
                            }
                        }
                        if(tailleSrcSecond > m_BackupFolderMaxSpace){
                            if(!fileToRemove.remove(m_BackupFolder+"/"+stringlistBackup.at(0))){
                                qStdout() << "impossible to remove : " << stringlistBackup.at(0)<<Qt::endl;
                            }
                        }
                    }while(tailleSrcSecond > m_BackupFolderMaxSpace);
                    if(!fileToRemove.copy(m_folder+"/"+stringlist.at(0),m_BackupFolder+"/"+stringlist.at(0))){
                        qStdout() << "impossible to copy : " << stringlist.at(0)<<Qt::endl;
                    }
                }
                stringlist = uploadDir.entryList(QDir::Files | QDir::NoDotAndDotDot,QDir::Time | QDir::Reversed);
                if(!fileToRemove.remove(m_folder+"/"+stringlist.at(0))){
                    qStdout() << "impossible to remove : " << stringlist.at(0)<<Qt::endl;
                }
            }
        }while(tailleSrcMain > m_FolderMaxSpace);   
}


bool FileUploader::fileNoLoraNoSigfoxNoGz(QString string){
    return (!string.endsWith(".gz") && !string.endsWith(".lora") && !string.endsWith(".sigfox"));
}

int FileUploader::askNetwork()
{
    //TODO
    m_noNetwork = 0;
    return 0;
}


void FileUploader::quit()
{
    m_running = 0;
    timeoutUpload->stop();
    emit quitSignalAll();
}
