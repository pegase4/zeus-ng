#include <QFile>
#include <QDebug>
#include <QJsonDocument>
#include <QTextStream>
#include "config.h"
#include "debug.h"

Config::Config()
{
}

bool Config::load(QString filepath)
{
    bool rc;

    qStdout() << "Using configuration file:" << filepath << Qt::endl;

    /*
     *  Open the file
     */
    QFile file(filepath);
    rc = file.open(QFile::ReadOnly);
    if (rc == false) {
        qStdout() << "The configuration file is not readable" << Qt::endl;
        return false;
    }

    /*
     *  Initilize Json Document
     */
    QString content = file.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(content.toUtf8());
    if (doc.isNull()) {
        qStdout() << "The configuration file is not JSON valid" << Qt::endl;
        return false;
    }

    /*
     *  Ensure the configuration file is an object
     */
    if (doc.isObject() == false) {
        qStdout() << "The configuration file is not a JSON Object" << Qt::endl;
        return false;
    }
    m_json = doc.object();

    return true;
}

void Config::show()
{
    qStdout() << "Configuration summary" << Qt::endl;
    qStdout() << "\tbroker : "  << Qt::endl;
    qStdout() << "\t\taddress : " << getBrokerAdress() << Qt::endl;
    qStdout() << "\t\tport : " << getBrokerPort() <<Qt::endl;
    qStdout() << "\tupload : " << Qt::endl;
    qStdout() << "\t\tfolder : " << getUploadFolder() << Qt::endl;
    qStdout() << "\t\tfolder max space (octets) : " << getUploadFolderMaxSpace() << Qt::endl;
    qStdout() << "\t\tBackupFolder : " << getBackupUploadFolder() << Qt::endl;
    qStdout() << "\t\ttBackupFolder max space (octets) : " << getBackupFolderMaxSpace() << Qt::endl;
    qStdout() << "\t\ttCompacting speed : " << getCompactingSpeed() << Qt::endl;
    qStdout() << "\t\turl : "<< getUploadUrl()  << Qt::endl;
    qStdout() << "\t\tperiodUpload : " << getUploadPeriod() << Qt::endl;
}

QString Config::getBrokerAdress()
{
    QJsonValue value = m_json["broker"];
    QJsonObject object = value.toObject();
    return object["address"].toString();
}

int Config::getBrokerPort()
{
    QJsonValue value = m_json["broker"];
    QJsonObject object = value.toObject();
    return object["port"].toInt();
}

QString Config::getUploadFolder()
{
    QJsonValue value = m_json["upload"];
    QJsonObject object = value.toObject();
    return object["folder"].toString();
}

int Config::getUploadFolderMaxSpace(){
	QJsonValue value = m_json["upload"];
	QJsonObject object = value.toObject();
	return object["folderMaxSpace"].toInt();
}

QString Config::getBackupUploadFolder()
{
	QJsonValue value = m_json["upload"];
	QJsonObject object = value.toObject();
	return object["BackupFolder"].toString();
}

int Config::getBackupFolderMaxSpace(){
	QJsonValue value = m_json["upload"];
	QJsonObject object = value.toObject();
	return object["BackupFolderMaxSpace"].toInt();
}

QString Config::getUploadUrl()
{
    QJsonValue value = m_json["upload"];
    QJsonObject object = value.toObject();
    return object["url"].toString();
}

int Config::getUploadPeriod()
{
    QJsonValue value = m_json["upload"];
    QJsonObject object = value.toObject();
    return object["periodUpload"].toInt();
}

int Config::getCompactingSpeed()
{
    QJsonValue value = m_json["upload"];
    QJsonObject object = value.toObject();
    return object["compactingSpeed"].toInt();
}

