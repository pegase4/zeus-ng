#ifndef FILEUPLOADER_H
#define FILEUPLOADER_H

#include <QObject>
#include <QDir>
#include <debug.h>
#include <QUrl>
#include <QProcess>
#include <sender.h>
#include <config.h>
#include <unistd.h>
#include <QTimer>

#define SPACE_MEMORY_MAIN 150000000 //300 m0 (all the PEGASE3 NAND)
#define SPACE_MEMORY_SECOND 1000000000 //1 Go (a single gO for the SD Card)

class FileUploader : public QObject
{
    Q_OBJECT

public:

    FileUploader(Config myConf,QString mac);
    ~FileUploader();
    int start();

public slots:
    void quit();
    void uploadFileTimeout();
signals:
    void quitSignalAll();
    void endWaitNetwork();
    void endWaitLpwan();

private:
    enum state {SEND_GZ = 0,SEND_LORA,SEND_SIGFOX,WAIT,TOO_OLD};

    QString m_addressBroker;
    int m_portBroker;
	int m_FolderMaxSpace=0;
	int m_BackupFolderMaxSpace=0;
    int m_CompactingSpeed=6;
    QString m_mac;
    QString m_folder;
	QString m_BackupFolder;
    QString m_url;
    int m_periodUpload;
    int m_running = 1;
    bool m_noNetwork = 0;
    FileUploader::state m_state = SEND_GZ;

    bool fileNoLoraNoSigfoxNoGz(QString string);
    int askNetwork();

    QTimer * timeoutUpload ;

};

#endif // FILEUPLOADER_H
