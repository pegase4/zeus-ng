#include <QCoreApplication>
#include <iostream>
#include <QProcess>
#include <QUrl>
#include <unistd.h>
#include <sender.h>
#include <QNetworkInterface>
#include <config.h>
#include <debug.h>
#include <fileuploader.h>
#include <signal.h>
using namespace std;

QString findMac(void);

FileUploader *fileuploader;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("Zeus Upload");
    QCoreApplication::setApplicationVersion("1.0.0");



    /*
     *  Load configuration
     */
    Config conf;
    bool rc = conf.load("/etc/zeus/upload.json");
    if (rc == false) {
        qStdout() << "Fallback on build-in configuration" << Qt::endl;
        rc = conf.load(":/upload.json");
        if (rc == false) {
            qStdout() << "Can not load configuration" << Qt::endl;
            return -1;
        }
    }
    conf.show();
    /*
     *  Find mac wlan0
     */
    QString mac = findMac();
    fileuploader = new FileUploader(conf,mac);
    fileuploader->start();
    return a.exec();
}

QString findMac(void){
    QString mac = "NONE";
    QString pathFileConf ="/sys/class/net/mlan0/address" ;
    QFile file(pathFileConf);
    if (!file.open(QIODevice::ReadOnly)){
        qDebug() <<"error open "<< pathFileConf <<Qt::endl;
        return mac;
    }
    QTextStream in(&file);
    mac = in.readAll();
    mac.remove("\n");
    qStdout() << " mac :" << qPrintable(mac) << Qt::endl;
    return mac;
}
