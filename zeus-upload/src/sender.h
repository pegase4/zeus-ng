#ifndef SENDER_H
#define SENDER_H

#include <QNetworkReply>
#include <QObject>
#include <QHttpMultiPart>
#include <QFile>
#include <QCoreApplication>
#include <debug.h>
#include "zeusbustimeout.h"
enum ReturnEnvoi {
    ABORT,
    TIMEOUT,
    ERROR,
    OK
};

class Sender : public QObject
{
    Q_OBJECT

public:
    Sender();
    Sender(QUrl url);
    ~Sender();
    ReturnEnvoi sendFile(QString pathFile, QString nameFile);

signals:
    void abortSignal();

public slots:

    void enregistrer();
    void abort();
    void progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal);
    void messageErreur(QNetworkReply::NetworkError);

private:
    QUrl m_url;
    QNetworkAccessManager m_nam;
    int m_abort = 0;
    int m_timeout = 0;
	bool m_fileSend = true;
    int m_signalsErrorCaught = 0;
    int m_signalsfinishCaught = 0;
    QEventLoop::ProcessEventsFlags m_eventFlags;
};

#endif // SENDER_H
