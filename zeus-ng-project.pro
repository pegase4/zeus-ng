TEMPLATE = subdirs
SUBDIRS =

ZTE=$$(ZEUS_TIME_ENABLE)
equals(ZTE,true){
    SUBDIRS += zeus-time
    message(zeus-time activated : $$ZTE)
}else{
    message(zeus-time activated : $$ZTE)
}

ZAE=$$(ZEUS_APPS_ENABLE)
equals(ZAE,true){
    SUBDIRS += zeus-apps
    message(zeus-apps activated : $$ZAE)
}else{
    message(zeus-apps activated : $$ZAE)
}

ZLE=$$(ZEUS_LOG_ENABLE)
equals(ZLE,true){
    SUBDIRS += zeus-log
    message(zeus-log activated : $$ZLE)
}else{
    message(zeus-log activated : $$ZLE)
}

ZUE=$$(ZEUS_UPLOAD_ENABLE)
equals(ZUE,true){
    SUBDIRS += zeus-upload
    message(zeus-upload activated : $$ZUE)
}else{
    message(zeus-upload activated : $$ZUE)
}
