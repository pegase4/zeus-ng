TEMPLATE = aux

#
#   Deploy user config file
#
customconfig.path = $$[QT_INSTALL_PREFIX]/../etc/zeus
customconfig.files = apps.json
INSTALLS += customconfig

#   Deploy default config file
#
defaultconfig.path = $$[QT_INSTALL_PREFIX]/share/zeus
defaultconfig.files = apps.json
INSTALLS += defaultconfig

