#ifndef APPSMANAGEUR_H
#define APPSMANAGEUR_H
#include <QObject>
#include <config.h>
#include <QDebug>
#include <loaderappsconf.h>
#include <QProcess>



class AppsManageur: public QObject
{
    Q_OBJECT
public:
    enum state {LOAD_CONF,KILL_ALL,START_DIFF,WAIT};
    AppsManageur(Config myConfig,QString mac);
    ~AppsManageur();
    int start();

public slots:
   void appsFinished();
   void quiAll();
   void killAllApps();

signals:
   void quitSignal();
private:
    QString m_addressBroker;
    int m_portBroker;
	QString m_WorkingDirectory;
    QString m_mac;
    QString m_url;
    int m_periodDownload;
    bool m_oneShot;
    QString m_appMode;
    int m_running = 1;
    state m_state = LOAD_CONF;
    QList<App*> m_listApps;

    QList<App::confApp> readConfApps();

    void showAppsConf(QList<App::confApp> listAppConf);
    bool starApp(App* app);
};

#endif // APPSMANAGEUR_H
