#ifndef APP_H
#define APP_H
#include <QObject>
#include <qstringlist.h>
#include <QProcess>
#include <QDebug>
#include <string>
#include <debug.h>

class App: public QObject
{
    Q_OBJECT
public:
    struct confApp{
        QString commandLine;
        QString statusApp;
        QStringList cartefilleList;
        QStringList driverList;
        QStringList fileList;
        int periodUpload;
        QStringList show(){
            QStringList ret;
            ret << "Commande Line : "+commandLine;
            ret << "Status : "+statusApp;
            ret << "Periode upload : "+QString::number(periodUpload);
            for(int i = 0; i < cartefilleList.size(); i++){
                ret << "Carte fille list : "+cartefilleList.at(i);
            }
            for(int i = 0; i < driverList.size(); i++){
                ret << "driver list : "+driverList.at(i);
            }
            for(int i = 0; i < fileList.size(); i++){
                ret << "mda5 file list : "+fileList.at(i);
            }

            return ret;
        }
        bool operator== (const confApp& b) const
        {
            return  (commandLine == b.commandLine) && (statusApp == b.statusApp)&& (fileList == b.fileList) && (cartefilleList == b.cartefilleList) && (driverList == b.driverList && (periodUpload == b.periodUpload));
        }
    };

    App(confApp myConf);

    void start();
    void stop();
    confApp myConf() const;
    void setMyConf(const confApp &myConf);

    QProcess *Process() const;
    void setProcess(QProcess *Process);

public slots:
    void quit();
    void appEndSlot(int i, QProcess::ExitStatus exit);
    void readyReadStandardOutput();
    void readyReadStandardError();
signals:
    void appEndSignal(int, QProcess::ExitStatus);

private :
    confApp m_myConf;
    QProcess *m_Process;
};


#endif // APP_H
