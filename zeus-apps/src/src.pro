#
#   Qt modules
#
QT += core
QT += network
QT -= gui
QT += xml

#
#   Project configuration and compiler options
#
CONFIG += c++11
CONFIG += warn_on
#CONFIG += console

#
#   Output config
#
TEMPLATE = app
TARGET = zeus-apps
target.path = $$[QT_INSTALL_BINS]
INSTALLS += target

#
#   Sources
#
SOURCES += \
    main.cpp \
    debug.cpp \
    config.cpp \
    appsmanageur.cpp \
    loaderappsconf.cpp \
    zeusbustimeout.cpp\
    app.cpp

HEADERS += \
    debug.h \
    config.h \
    appsmanageur.h \
    loaderappsconf.h \
    zeusbustimeout.h\
    app.h

RESOURCES += \
    bundle.qrc
