#ifndef LOADERAPPSCONF_H
#define LOADERAPPSCONF_H
#include <QObject>
#include <QList>
#include <QNetworkAccessManager>
#include <qnetworkrequest.h>
#include <QNetworkReply>
#include <QDomDocument>
#include <QFile>
#include <QFileInfo>
#include <QDebug>
#include <QDir>
#include <app.h>
#include "zeusbustimeout.h"
class LoaderAppsConf: public QObject
{
    Q_OBJECT
public:
    enum statusConf{OK,KO,NO_INTERNET};

    LoaderAppsConf(QString WorkingDirectory, QString url, QString mac, QString appMode);
    LoaderAppsConf:: statusConf loadAppsConf(QList<App::confApp> *listReturn);

signals:
    void endWaitNetwork();
    void quitSignalAll();
    void errorConf();
public slots:

    void enregistrer();
    void progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal);
    void messageErreur(QNetworkReply::NetworkError);
    void quit();
private:
    QString m_mac;
    QString m_url;
	QString m_WorkingDirectory;
    QString m_appMode;
    QNetworkAccessManager m_nam;
    bool m_noNetwork = 0;
    bool m_haveConf = true;



    int askNetwork();
};

#endif // LOADERAPPSCONF_H
