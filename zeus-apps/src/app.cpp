#include "app.h"

App::App(confApp myConf)
{
    m_myConf = myConf;
    m_Process = new QProcess();
}

void App::start()
{
    connect(m_Process,SIGNAL(finished(int,QProcess::ExitStatus)),this,SLOT(appEndSlot(int,QProcess::ExitStatus)));
    connect(m_Process, SIGNAL(readyReadStandardOutput()), this, SLOT(readyReadStandardOutput()));
    connect(m_Process, SIGNAL(readyReadStandardError()), this, SLOT(readyReadStandardError()));

    m_Process->start(m_myConf.commandLine);
    qStdout() <<"start app : "<< m_myConf.commandLine << Qt::endl;

}

void App::stop()
{
    m_Process->kill();
}
App::confApp App::myConf() const
{
    return m_myConf;
}

void App::setMyConf(const confApp &myConf)
{
    m_myConf = myConf;
}

void App::quit()
{

}

void App::appEndSlot(int i, QProcess::ExitStatus exit)
{
    emit appEndSignal(i,exit);
}

void App::readyReadStandardOutput()
{
    QProcess *p = (QProcess *)sender();
    QByteArray buf = p->readAllStandardOutput();
    if(buf.size() != 0){
        qStdout() <<m_myConf.commandLine.split(' ').at(0)<< " = " << buf<< Qt::endl;
    }
}

void App::readyReadStandardError()
{
    QProcess *p = (QProcess *)sender();
    QByteArray buf = p->readAllStandardError();
    if(buf.size() != 0){
        qStdout() <<" [APPS_ERROR] " << m_myConf.commandLine.split(' ').at(0)<< " = " << buf<< Qt::endl;
    }
}
QProcess *App::Process() const
{
    return m_Process;
}

void App::setProcess(QProcess *Process)
{
    m_Process = Process;
}


