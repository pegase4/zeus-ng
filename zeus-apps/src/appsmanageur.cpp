#include "appsmanageur.h"

AppsManageur::AppsManageur(Config myConfig, QString mac)
{
    m_mac = mac;
    m_WorkingDirectory = myConfig.getWorkingDirectory();
    m_url = myConfig.getAppsUrl();
    m_periodDownload = myConfig.getAppsPeriodDownload();
    m_oneShot = myConfig.getAppsOneShot();
    m_appMode = myConfig.getAppsMode();
    m_state = LOAD_CONF;
    //m_zbus = new ZeusBus(myConfig.getBrokerAdress(), myConfig.getBrokerPort());
}

AppsManageur::~AppsManageur()
{
    killAllApps();
    m_running = 0;
    emit quitSignal();
}

int AppsManageur::start()
{
    ZeusBusTimeout wait10sec;
    wait10sec.wait(10 * 1000);//attendre que zeus-network se lance
    LoaderAppsConf loader(m_WorkingDirectory, m_url,m_mac, m_appMode);
    QList<App::confApp> listAppsConfNew;
    while(m_running){
        switch (m_state) {
        case LOAD_CONF:{
            LoaderAppsConf::statusConf status =  loader.loadAppsConf(&listAppsConfNew);
            switch (status) {
            case LoaderAppsConf::OK:
                m_state = START_DIFF;
                break;
            case LoaderAppsConf::KO:
                m_state = KILL_ALL;
                break;
            case LoaderAppsConf::NO_INTERNET:
                m_state = WAIT;
                break;
            default:
                m_state = WAIT;
                break;
            }
            //showAppsConf(listAppsConfNew);
            break;
        }
        case KILL_ALL:{
            m_state = WAIT;
            killAllApps();
            break;
        }
        case START_DIFF:{
            m_state = WAIT;
            foreach (App* app, m_listApps) {
                bool find = false;
                foreach (App::confApp confApp, listAppsConfNew) {
                    if(app->myConf() == confApp){
                        find = true;
                    }
                }
                if(!find){
                    app->stop();
                    m_listApps.removeOne(app);
                }
            }
            foreach (App::confApp confApp, listAppsConfNew) {
                bool find = false;
                foreach (App* app, m_listApps) {
                    if(app->myConf() == confApp){
                        find = true;
                    }
                }
                if(!find){
                    App *newApp = new App(confApp);
                    if(starApp(newApp)){
                        m_listApps.push_back(newApp);
                        qDebug() <<"start " <<newApp->myConf().commandLine;

                    }else{
                        qDebug() <<"fail to start " <<newApp->myConf().commandLine;
                    }

                }
            }
            break;
        }
        case WAIT:{
            m_state = LOAD_CONF;
            if(m_running){
                ZeusBusTimeout zeusTimeout;
                zeusTimeout.addExitSignal(this,SIGNAL(quitSignal()));
                zeusTimeout.wait(m_periodDownload * 1000);
            }
            break;
        }
        default:
            m_state = LOAD_CONF;
            break;
        }
        //qDebug() <<"State "<< m_state;
    }
    return 0;
}

void AppsManageur::appsFinished()
{
    App *appHaveEnd = qobject_cast<App*>(sender());
    qStdout() <<"Apps Finished "<< appHaveEnd->myConf().commandLine <<Qt::endl;
    if(m_running){
        ZeusBusTimeout zeusTimeout;
        zeusTimeout.addExitSignal(this,SIGNAL(quitSignal()));
        zeusTimeout.wait(30000);
        if(m_listApps.contains(appHaveEnd)){
            if(!m_oneShot){
                if(!starApp(appHaveEnd)){
                    m_listApps.removeOne(appHaveEnd);
                }
            }
        }
    }
}

void AppsManageur::killAllApps()
{
    foreach (App* app, m_listApps) {
        app->stop();
        m_listApps.removeOne(app);
    }
}


void AppsManageur::quiAll()
{
    m_state = KILL_ALL;
    emit quitSignal();
    m_running = 0;
}

void AppsManageur::showAppsConf(QList<App::confApp> listAppConf)
{
    qDebug() << "Apps conf : " <<Qt::endl;
    for(int i =0; i < listAppConf.size(); i++){
        App::confApp test = listAppConf.at(i);
        qDebug() << "Apps "<<i<<" " << test.show().join("\n") <<Qt::endl;
    }
}

bool AppsManageur::starApp(App *app)
{
    connect(app, SIGNAL(appEndSignal(int, QProcess::ExitStatus)), this, SLOT(appsFinished()));
    app->start();
    return true;
}

