#include "loaderappsconf.h"
#include <unistd.h>

LoaderAppsConf::LoaderAppsConf(QString WorkingDirectory, QString url, QString mac, QString AppMode)
{
    m_WorkingDirectory = WorkingDirectory;
    if(m_WorkingDirectory.at(m_WorkingDirectory.size()-1)!=(QString)"/"){
        m_WorkingDirectory.append("/");
    }
    if(m_WorkingDirectory.at(0)!=(QString)"/"){
        m_WorkingDirectory= "/" + m_WorkingDirectory;
    }
    m_url = url;
    m_mac = mac;
    m_appMode = AppMode;
   if(m_appMode=="local"){
        if(QFileInfo(m_WorkingDirectory+"zeusAppDownload.txt").exists()){
            qDebug() << "The zeusAppDownload file already exists, using old one";
        }
        else{
            QFile::copy("/usr/share/zeus/zeusAppDownload.txt", m_WorkingDirectory);
        }
    }
}

LoaderAppsConf:: statusConf LoaderAppsConf::loadAppsConf(QList<App::confApp> *listReturn)
{
    m_haveConf = false;
    if(m_appMode == "network"){
        QString urlConf = m_url+"/api/std/v1/config/mac="+m_mac+"/";
        qDebug() << urlConf;
        QUrl url(urlConf);
        QNetworkRequest request(url);
        QNetworkReply *reply;
		QSslConfiguration conf = request.sslConfiguration();
		conf.setPeerVerifyMode(QSslSocket::VerifyNone);
		request.setSslConfiguration(conf);
		reply = m_nam.get(request);
		connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(messageErreur(QNetworkReply::NetworkError)));
		connect(reply, SIGNAL(finished()), this, SLOT(enregistrer()));
		connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progressionTelechargement(qint64, qint64)));
		m_haveConf = true;
		ZeusBusTimeout zeusTimeout;
		zeusTimeout.addContinueSignal(reply,SIGNAL(downloadProgress(qint64, qint64)));
		zeusTimeout.addExitSignal(this, SIGNAL(errorConf()));
		zeusTimeout.addExitSignal(reply, SIGNAL(finished()));
		zeusTimeout.wait(10000);
	}
    QString pathFileConf;
    if(m_haveConf){
        pathFileConf =m_WorkingDirectory+"zeusAppDownloadNew.txt";
    }else{
        pathFileConf =m_WorkingDirectory+"zeusAppDownload.txt";
    }
    QDomDocument doc;
    QFile file(pathFileConf);
    if (!file.open(QIODevice::ReadOnly)){
        qStdout() <<"error open "<< pathFileConf <<Qt::endl;
        return NO_INTERNET;
    }
    QTextStream in(&file);
    QString text = in.readAll();
    file.close();
    if (text.indexOf("KO")==0){
        listReturn->clear();
        qDebug() <<"error fichier begin by KO " <<Qt::endl;
        return KO;
    }
    text = text.insert(text.indexOf("\n"),"<Applications>")+"</Applications>";
    if (!doc.setContent(text)){
        qStdout() <<"error setContent : "<< text<<Qt::endl;
        return NO_INTERNET;
    }
    listReturn->clear();
    qWarning() << text<< Qt::endl;;
    QDomNodeList Applications = doc.elementsByTagName("Application");
    // qDebug() <<"application size "<< Applications.size() <<Qt::endl;
    for (int i = 0; i < Applications.size(); i++) {
        App::confApp tempConfApp;
        QStringList fileList;
        QDomNode n = Applications.item(i);
        QDomElement commande = n.firstChildElement("Commande");
        QDomElement status = n.firstChildElement("Status");
        QDomElement carteFille = n.firstChildElement("CarteFille");
        QDomElement periodUpload = n.firstChildElement("PeriodUpload");
        QDomElement driver = n.firstChildElement("Driver");
        QDomNodeList Files = n.childNodes();
        for (int i = 0; i < Files.size(); i++) {
            QDomNode nodeFile = Files.item(i);
            if(nodeFile.toElement().tagName() == "FILE"){
                QStringList listDir = nodeFile.toElement().attribute("filepath").split("/");
                fileList.push_back(nodeFile.toElement().attribute("md5"));
                listDir.removeLast();
                QDir dir;
                dir.mkpath(listDir.join("/"));
                QFile file(nodeFile.toElement().attribute("filepath"));
                file.open(QIODevice::WriteOnly);
                QTextStream flux(&file);
                file.close();
                flux << QByteArray::fromBase64(nodeFile.toElement().text().toLatin1());
            }
        }
        tempConfApp.fileList = fileList;
        qDebug()<< commande.text()<< Qt::endl;;
        tempConfApp.commandLine = commande.text();
        tempConfApp.statusApp = status.text();
        tempConfApp.periodUpload = periodUpload.text().toInt();
        tempConfApp.cartefilleList = carteFille.text().split(',');
        tempConfApp.driverList = driver.text().split(',');
        tempConfApp.cartefilleList.removeAll("");
        tempConfApp.driverList.removeAll("");
        listReturn->push_back(tempConfApp);
    }
    if(m_haveConf && m_appMode=="network"){
        /*if(!QFile::remove(m_WorkingDirectory+"zeusAppDownload.txt")){
            qStdout() << "can't remove " << m_WorkingDirectory << "zeusAppDownload.txt";
        }
        if(!QFile::copy (m_WorkingDirectory+"zeusAppDownloadNew.txt", m_WorkingDirectory+"zeusAppDownload.txt")){
            qStdout() << "can't copy " << m_WorkingDirectory << "zeusAppDownloadNew.txt to" << m_WorkingDirectory << "zeusAppDownload.txt";
        }*/

        QFile file(m_WorkingDirectory+"zeusAppDownloadNew.txt");
        if (!file.open(QIODevice::ReadOnly)){
            qStdout() <<"error open "<< pathFileConf <<Qt::endl;
            return NO_INTERNET;
        }
        QTextStream in(&file);
        QString text = in.readAll();
        file.close();

        QFile f(m_WorkingDirectory+"zeusAppDownload.txt"); //On ouvre le fichier
        if ( f.open(QIODevice::WriteOnly) )
        {
            f.write(text.toUtf8()); ////On lit la réponse du serveur que l'on met dans un fichier
            f.close(); //On ferme le fichier
        }

    }

    int periodUploadMin = 900;
    for(int appNum = 0; appNum < listReturn->size();appNum++){
        if(listReturn->at(appNum).periodUpload < periodUploadMin){
            periodUploadMin = listReturn->at(appNum).periodUpload;
        }
    }
    return OK;
}

void LoaderAppsConf::enregistrer()
{
    //On vérifie qu'il n'y a pas eu d'erreur.
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender()); //On récupère la réponse du serveur
    QByteArray reponse = r->readAll();
    if(reponse.isEmpty()){
        m_haveConf = false;
    }else{
        QString pathFileConf =m_WorkingDirectory+"zeusAppDownloadNew.txt" ;
        QFile f(pathFileConf); //On ouvre le fichier
        if ( f.open(QIODevice::WriteOnly) )
        {
            f.write(reponse); ////On lit la réponse du serveur que l'on met dans un fichier
            f.close(); //On ferme le fichier
            r->deleteLater(); //IMPORTANT : on emploie la fonction deleteLater() pour supprimer la réponse du serveur.
            qDebug()<< "fichier conf telecharge"<<Qt::endl;
        }
    }

}

void LoaderAppsConf::progressionTelechargement(qint64 bytesReceived, qint64 bytesTotal)
{
    if(bytesReceived != 0 && bytesTotal!=0){
        // qDebug() << bytesReceived << " / "<< bytesTotal<< Qt::endl;
    }
}

void LoaderAppsConf::messageErreur(QNetworkReply::NetworkError)
{
    m_haveConf = false;
    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender());
    qStdout() << r->errorString() << Qt::endl;
    emit errorConf();
}

void LoaderAppsConf::quit()
{

    qStdout() << "ctrl c"<<Qt::endl;
    emit quitSignalAll();

}
