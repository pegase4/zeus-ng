#ifndef ZEUSBUSTIMEOUT_H
#define ZEUSBUSTIMEOUT_H

#include <QPointer>
#include <QObject>
#include <QTimer>
#include <QEventLoop>
#include <QElapsedTimer>
#include <QUrl>

class ZeusBusTimeout : public QObject
{
    Q_OBJECT

public:
    explicit ZeusBusTimeout(QObject *parent = 0);
    explicit ZeusBusTimeout(QUrl url, QObject *parent = 0);
    void addExitSignal(QObject *sender, const char *sig);
    void addContinueSignal(QObject *sender, const char *sig);

    bool wait(int msec = -1);


    int timeElapsed() const;

public slots:
    void onExit(QUrl url);
    void onContinue(QUrl url);


private slots:
    void onExit();
    void onContinue();

private:
    QEventLoop *m_pause;
    QTimer *m_timer;
    int m_timeout;
    bool m_rc;
    QElapsedTimer m_chrono;
    int m_timeElapsed;
    QUrl m_url;
};

#endif // ZEUSBUSTIMEOUT_H
