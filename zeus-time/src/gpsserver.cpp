#include "gpsserver.h"
#include "debug.h"

GPSServer::GPSServer(QObject *parent) : QObject(parent)
{
    m_server = new QTcpServer(this);
    connect(m_server, SIGNAL(newConnection()), this, SLOT(newConnection()));
}

void GPSServer::sendData(QByteArray payload)
{
    foreach (QTcpSocket *client, m_client) {
        client->write(payload);
        client->flush();
    }
}

bool GPSServer::start(int port)
{
    bool rc;

    rc = m_server->listen(QHostAddress::Any, port);
    if (rc == true) {
        qStdout() << "TCP GPSServer started on port " << port << Qt::endl;
    } else {
        qStdout() << "TCP GPSServer failed to start on port " << port << Qt::endl;
    }

    return rc;
}

void GPSServer::newConnection()
{
    QTcpSocket *client = m_server->nextPendingConnection();
    if (client == Q_NULLPTR) {
        return;
    }
    qDebug().nospace() << "Connection from: " << client->peerAddress().toString() << ":" << client->peerPort();

    connect(client, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(client, SIGNAL(disconnected()), this, SLOT(disconnected()));
    m_client << client;
}

void GPSServer::disconnected()
{
    QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
    qDebug().nospace() << "Disconnect from: " << client->peerAddress().toString() << ":" << client->peerPort();

    m_client.removeOne(client);
    client->deleteLater();
}

void GPSServer::readyRead()
{
    QByteArray data;
    QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());

    do {
        data = client->readLine();
        int size = data.size();

        // Read line will return a empty buffer, if we can not read a full line
        if (size == 0) {
            break;
        }

        emit dataAvailable(data);
    } while(true);
}
