#ifndef UBLOX_H
#define UBLOX_H

#include <QObject>
#include <QIODevice>
#include <QPointer>
#include <QByteArray>
#include <QSerialPort>

class UbloxFrame : public QByteArray
{
public:
    void append(int value);
    void appendInt32(int value);
	void appendInt16(int value);

    void appendHeader(void);
    void appendChecksum(void);

    static UbloxFrame parseUbxLine(QString line);
};

class Ublox : public QObject
{
    Q_OBJECT

public:
    explicit Ublox(QSerialPort *device, QObject *parent = nullptr);
    void setDeviceBaudrate(qint32 baudRate);
    void configureReceiver();

    void setSurveyDuration(unsigned int duration);
    void setSurveyAccuracy(unsigned int accuracy);

public slots:
    void resetConfiguration(void);
    void setBaudrate(int baudrate);
    bool applyConfigurationFile(QString filepath);

    // Fixed mode
	bool enterFixedMode(void);
	bool enterFixedMode(int x, int y, int z, int v);
    bool clearFixedConfiguration(void);
    bool updateFixedConfiguration(int x, int y, int z, int v);
	void antennaAndPPSConfiguration(void);


    // Survey-in mode
    void enterSurveyInMode(void);
    void enterSurveyInMode(unsigned int duration, unsigned int accuracy);

private:
    unsigned int m_duration;
    unsigned int m_accuracy;
    QPointer<QSerialPort> m_device;
    static const QString fixedFilePath;
};

#endif // UBLOX_H
