#ifndef UBX_H
#define UBX_H

#include <cstdint>

/*
 *  Define content of the UBX payload
 */

/* UBX 0x0D01 */
#define TIM_TP                      0x0D01
struct ubx_tim_tp
{
    uint32_t towMS;
    uint32_t towSubMS;
    int32_t  qErr;
    uint16_t week;
    uint8_t  flags;
    uint8_t  reserved1;
} __attribute__ ((__packed__));

/* UBX 0x0D04 */
#define TIM_SVIN                    0x0D04
struct ubx_tim_svin
{
    uint32_t dur;
    int32_t  meanX;
    int32_t  meanY;
    int32_t  meanZ;
    uint32_t meanV;
    uint32_t obs;
    uint8_t  valid;
    uint8_t  active;
    uint16_t reserved1;
} __attribute__ ((__packed__));

/* UBX 0x0103 */
#define NAV_STATUS                  0x0103
struct ubx_nav_status
{
    uint32_t iTOW;
    uint8_t gpsFix;
#define NAV_STATUS_GPSFIX_NOFIX     0
#define NAV_STATUS_GPSFIX_DRONLY    1
#define NAV_STATUS_GPSFIX_2D        2
#define NAV_STATUS_GPSFIX_3D        3
#define NAV_STATUS_GPSFIX_3DDR      4
#define NAV_STATUS_GPSFIX_TIME      5

    uint8_t flags;
#define NAV_STATUS_FLAGS_FIXOK      (1 << 0)
#define NAV_STATUS_FLAGS_DGPS       (1 << 1)
#define NAV_STATUS_FLAGS_WKN        (1 << 2)
#define NAV_STATUS_FLAGS_TOW        (1 << 3)

    uint8_t fixStat;
    uint8_t flags2;
    uint32_t ttff;
    uint32_t msss;
} __attribute__ ((__packed__));

/* UBX 0x0121 */
#define NAV_TIMEUTC                 0x0121
struct ubx_nav_timeutc
{
    uint32_t iTOW;
    uint32_t tAcc;
    int32_t  nano;
    uint16_t year;
    uint8_t  month;
    uint8_t  day;
    uint8_t  hour;
    uint8_t  min;
    uint8_t  sec;
    uint8_t  valid;
#define NAV_TIMEUTC_VALID_TOW       (1 << 0)
#define NAV_TIMEUTC_VALID_WKN       (1 << 1)
#define NAV_TIMEUTC_VALID_UTC       (1 << 2)
} __attribute__ ((__packed__));

/* UBX 0x0106 */
#define NAV_SOL		0x0106
struct ubx_nav_sol
{
        uint32_t iTOW;
        int32_t  fTOW;
        int16_t  week;
        uint8_t  gpsFix;
        uint8_t  flags;

#define NAV_SOL_FLAGS_GPSfixOK       (1 << 0)
#define NAV_SOL_FLAGS_DiffSoln       (1 << 1)
#define NAV_SOL_FLAGS_WKNSET         (1 << 2)
#define NAV_SOL_FLAGS_TOWSET         (1 << 3)

        int32_t  ecefX;
        int32_t  ecefY;
        int32_t  ecefZ;
        uint32_t pAcc;
        int32_t  ecefVX;
        int32_t	 ecefVY;
        int32_t	 ecefVZ;
        uint32_t sAcc;
        uint16_t pDOP;
        uint8_t  reserved1;
        uint8_t  numSV;
        uint32_t  reserved2;


} __attribute__ ((__packed__));

/* UBX 0x0130 */
#define NAV_SVINFO  0X0130
struct ubx_nav_svinfo
{
        uint32_t iTOW;
        uint8_t  numCh;
        uint8_t globalFlags;

#define NAV_SVINFO_GLOBALFLAGS_CHIPGEN       (1 << 2)

        uint16_t reserved;

        struct {
        uint8_t chn;
        uint8_t svid;
        uint8_t flags;

#define NAV_SVINFO_FLAGS_SVUSED		(1 << 0)
#define NAV_SVINFO_FLAGS_DIFFCORR	(1 << 1)
#define NAV_SVINFO_FLAGS_ORBITAVAIL	(1 << 2)
#define NAV_SVINFO_FLAGS_ORBITEPH	(1 << 3)
#define NAV_SVINFO_FLAGS_UNHEALTHY	(1 << 4)
#define NAV_SVINFO_FLAGS_ORBITALM	(1 << 5)
#define NAV_SVINFO_FLAGS_ORBITAOP	(1 << 6)
#define NAV_SVINFO_FLAGS_SMOOTHED	(1 << 7)

        uint8_t quality;

#define NAV_SVINFO_QUALITY_QUALITYIND	(1 << 3)

        uint8_t cno;
        int8_t  elev;
        int16_t azim;
        int32_t prRes;
        } sv[12];
} __attribute__ ((__packed__));

union ubx {
    struct ubx_tim_tp tim_tp;
    struct ubx_tim_svin tim_svin;
    struct ubx_nav_status nav_status;
    struct ubx_nav_timeutc nav_timeutc;
    struct ubx_nav_svinfo nav_svinfo;
    struct ubx_nav_sol nav_sol;
};

#endif // UBX_H
