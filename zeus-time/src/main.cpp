#include <QCoreApplication>
#include <QDebug>
#include "config.h"
#include "debug.h"
#include "gps.h"

int main(int argc, char *argv[])
{
    bool rc;

    /*
     *  Init Qt console app
     */
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("Zeus Time");
    QCoreApplication::setApplicationVersion("1.0.0");

    /*
     *  Load configuration
     */
    Config conf;
    rc = conf.load("/etc/zeus/time.json");
    if (rc == false) {
        qStdout() << "Fallback on build-in configuration" << Qt::endl;
        rc = conf.load(":/time.json");
        if (rc == false) {
            qStdout() << "Can not load configuration" << Qt::endl;;
            return -1;
        }
    }
    conf.show();

    /*
     *  Initialize time source
     */
    QString source = conf.getTimeSource();
    if (source == "gps") {
		system("systemctl stop systemd-timesyncd");
		system("systemctl disable systemd-timesyncd");
        GPS *gps = new GPS(conf.getTimeConfig(), false);
        rc = gps->start();
        if (rc == false) {
            qStdout() << "GPS Initialization failed, abording";
            return -1;
        }

        // FFS connect zeus bus
	}else if (source == "ntp") {
        qStdout() << "Enabling and using NTP debian service" << Qt::endl;;
		system("systemctl enable systemd-timesyncd &");
		system("systemctl daemon-reload &");
		system("systemctl start systemd-timesyncd &");

        GPS *gps = new GPS(conf.getTimeConfig(), true);
		rc = gps->start();
		if (rc == false) {
			qStdout() << "GPS Initialization failed, abording";
			return -1;
		}


	}

    /*
     *  Qt event loop
     */
    return a.exec();
}
