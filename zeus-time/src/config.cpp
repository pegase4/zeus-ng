#include <QFile>
#include <QDebug>
#include <QJsonDocument>
#include <QTextStream>
#include "config.h"
#include "debug.h"

Config::Config()
{
}

bool Config::load(QString filepath)
{
    bool rc;

    qStdout() << "Using configuration file:" << filepath << Qt::endl;;

    /*
     *  Open the file
     */
    QFile file(filepath);
    rc = file.open(QFile::ReadOnly);
    if (rc == false) {
        qStdout() << "The configuration file is not readable" << Qt::endl;;
        return false;
    }

    /*
     *  Initilize Json Document
     */
    QString content = file.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(content.toUtf8());
    if (doc.isNull()) {
        qStdout() << "The configuration file is not JSON valid" << Qt::endl;;
        return false;
    }

    /*
     *  Ensure the configuration file is an object
     */
    if (doc.isObject() == false) {
        qStdout() << "The configuration file is not a JSON Object" << Qt::endl;;
        return false;
    }
    m_json = doc.object();

    return true;
}

void Config::show()
{
    qStdout() << "Configuration summary" << Qt::endl;;
    qStdout() << "\tBroker Address: " << getBrokerAddress() << Qt::endl;;
    qStdout() << "\tBroker Port: " << getBrokerPort() << Qt::endl;;
    qStdout() << "\tTime source: " << getTimeSource() << Qt::endl;;
}

QString Config::getBrokerAddress()
{
    QJsonObject broker = m_json["broker"].toObject();
    return broker["address"].toString();
}

int Config::getBrokerPort()
{
    QJsonObject broker = m_json["broker"].toObject();
    return broker["port"].toInt();
}

QString Config::getTimeSource()
{
	QJsonObject time = m_json["time"].toObject();
	return time["source"].toString();
}

QString Config::getModelGPS()
{
	QJsonObject time = m_json["time"].toObject();
	return time["model"].toString();
}

QJsonObject Config::getTimeConfig()
{
    QJsonObject time = m_json["time"].toObject();
    return time[getTimeSource()].toObject();
}


QString Config::getNTPSource()
{
	QJsonObject time = m_json["time"].toObject();
	return time["NTPsource"].toString();
}
