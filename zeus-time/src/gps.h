#ifndef GPS_H
#define GPS_H

#include <QObject>
#include <QFile>
#include <QTimer>
#include <QPointer>
#include <QJsonObject>
#include <QtSerialPort>
#include <ifsttar_sync.h>
#include <cstdint>
#include "gpsserver.h"
#include "ublox.h"

class GPS : public QObject
{
    Q_OBJECT
public:
    explicit GPS(QJsonObject config, bool bNTP, QObject *parent = nullptr);
    bool start();
	bool isSurveyingFinished();
	int antennaLength() const;
	int antennaVelocity() const;
	int TImeIntervalPPS() const;
	int LengthPPS() const;
	QString modelGPS() const;

signals:

public slots:
	void syncDate();
	void checkFixedStatus();

private slots:
    void uartReadyRead();
    void tcpDataAvailable(QByteArray payload);
    void createPositionFile();

private:
    bool isFixedMode() const;
    int surveyDuration() const;
    int surveyAccuracy() const;
    int positionFilePeriod() const;
    QString  positionFileFolder() const;

    uint8_t hex2int(char c) const;

    void parse(QByteArray payload);
    void ubxDecode(uint16_t ubx_id, QByteArray payload);
    void nmeaDecode(QByteArray payload);
    void nmeaDecodeGGA(QByteArray payload);
    void nmeaDecodeGSA(QByteArray payload);
    void nmeaDecodeZDA(QByteArray payload);
    void updateUnixTimestamp();

    int getDriverFd();

private:
    QJsonObject m_config;
    QTimer* m_positionFileTimer;
    QTimer* m_syncDateTimer;
	QTimer* m_waitingForFixedMode;
    Ublox* m_ubx;
    GPSServer* m_forward;
    QSerialPort* m_uart;

    QFile* m_driver;
    GpsData m_gpsdata;
    bool m_dateValid;
	bool m_ntp;
	static const QString fixedFilePath;
};

#endif // GPS_H
