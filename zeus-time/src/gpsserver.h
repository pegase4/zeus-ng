#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QString>
#include <QList>
#include <QTcpServer>
#include <QTcpSocket>
#include <QByteArray>

class GPSServer : public QObject
{
    Q_OBJECT

public:
    explicit GPSServer(QObject *parent = 0);
    bool start(int port = 5000);

signals:
    void dataAvailable(QByteArray payload);

public slots:
    void sendData(QByteArray payload);

private slots:
    // QTcpServer
    void newConnection();

    // QTcpSocket
    void disconnected();
    void readyRead();

private:
    QTcpServer *m_server;
    QList<QTcpSocket *> m_client;
};

#endif // SERVER_H
