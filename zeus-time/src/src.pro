#
#   Qt modules
#
QT += core
QT += network
QT += serialport
QT -= gui

#
#   Project configuration and compiler options
#
CONFIG += c++11
CONFIG += warn_on

#
#   Output config
#
TEMPLATE = app
TARGET = zeus-time
target.path = $$[QT_INSTALL_BINS]
INSTALLS += target

#
#   Sources
#
SOURCES += \
    main.cpp \
    config.cpp \
    debug.cpp \
    gpsserver.cpp \
    gps.cpp \
    ublox.cpp

HEADERS += \
    config.h \
    debug.h \
    gpsserver.h \
    gps.h \
    ublox.h \
    ubx.h

RESOURCES += \
    bundle.qrc

