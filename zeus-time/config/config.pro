TEMPLATE = aux

#
#   Deploy user config file
#
customconfig.path = $$[QT_INSTALL_PREFIX]/../etc/zeus
customconfig.files = time.json ublox.conf ublox_neo_6t.conf ublox_neo_8t.conf
INSTALLS += customconfig

#   Deploy default config file
#
defaultconfig.path = $$[QT_INSTALL_PREFIX]/share/zeus
defaultconfig.files = time.json
INSTALLS += defaultconfig

defaultconfigSourceNTP.path = $$[QT_INSTALL_PREFIX]/../etc/zeus
defaultconfigSourceNTP.files = ntp_sourceNTP.conf
INSTALLS += defaultconfigSourceNTP

defaultconfigSourceGPS.path = $$[QT_INSTALL_PREFIX]/../etc/zeus
defaultconfigSourceGPS.files = ntp_sourceGPS.conf
INSTALLS += defaultconfigSourceGPS


