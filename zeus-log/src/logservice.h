#ifndef LOGSERVICE_H
#define LOGSERVICE_H
#include <QTimer>
#include <QObject>
#include <QDebug>
#include <QProcess>
#include <QFile>
#include <QMutex>
#include <QDateTime>

class LogService : public QObject
{
    Q_OBJECT
public:
    explicit LogService(QObject *parent = 0);
    LogService(QString service, int periodUpload, int periodLoad, QString dirUpload, QString dirload, int sizeMaxLogFile);
    void start();
    void stop();

private:
    QString m_service;
    int m_periodUpload;
    int m_periodLoad;
    QString m_dirUpload;
    QString m_dirLoad;
    QString m_cursor;
    int m_sizeMaxLogFile;
    QTimer * m_timerUpload;
    QTimer * m_timerLoad;
    bool m_flagLoadLog;
    bool m_flagUploadLog;
    void changeCursor(QString cursor);
    void writeLog(QString log);
signals:

public slots:
    void refresh();
    void upload();

};

#endif // LOGSERVICE_H
