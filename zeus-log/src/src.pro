#
#   Qt modules
#
QT += core
QT += network
QT -= gui

#
#   Project configuration and compiler options
#
CONFIG += c++11
CONFIG += warn_on
CONFIG += console

#
#   Output config
#
TEMPLATE = app
TARGET = zeus-log
target.path = $$[QT_INSTALL_BINS]
INSTALLS += target


#
#   Sources
#
SOURCES += \
    main.cpp \
    config.cpp \
    debug.cpp \
    logmanager.cpp \
    logservice.cpp

HEADERS += \
    config.h \
    debug.h \
    logmanager.h \
    logservice.h

RESOURCES += \
    bundle.qrc


