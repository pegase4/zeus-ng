#include <QCoreApplication>
#include <iostream>
#include <QProcess>
#include <unistd.h>
#include <config.h>
#include <debug.h>
#include <logmanager.h>
#include <QDir>
#include <signal.h>

using namespace std;


LogManager *logManager;

void stop_callback(int signal)
{
    (void)(signal);
    logManager->stop();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("Zeus log");
    QCoreApplication::setApplicationVersion("1.1.5");

    signal (SIGTERM, stop_callback);


    /*
     *  Load configuration
     */
    Config conf;
    bool rc = conf.load("/etc/zeus/log.json");
    if (rc == false)
    {
        qDebug() << "Fallback on build-in configuration" << Qt::endl;
        rc = conf.load(":/log.json");
        if (rc == false)
        {
            qDebug() << "Can not load configuration" << Qt::endl;
            return -1;
        }
    }
    conf.show();

//    LogManager logManager(conf);
    logManager = new LogManager(conf);
    QObject::connect(logManager, SIGNAL(stopSignal()), &a, SLOT(quit()));
    logManager->start();

    qDebug() << "Zeus-log is executing" << Qt::endl;

    return a.exec();
}
