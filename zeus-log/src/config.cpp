#include <QFile>
#include <QDebug>
#include <QJsonDocument>
#include <QTextStream>
#include "config.h"
#include "debug.h"

Config::Config()
{
}

bool Config::load(QString filepath)
{
    bool rc;

    qStdout() << "Using configuration file:" << filepath << Qt::endl;

    /*
     *  Open the file
     */
    QFile file(filepath);
    rc = file.open(QFile::ReadOnly);
    if (rc == false)
    {
        qStdout() << "The configuration file is not readable" << Qt::endl;
        return false;
    }

    /*
     *  Initilize Json Document
     */
    QString content = file.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(content.toUtf8());
    if (doc.isNull())
    {
        qStdout() << "The configuration file is not JSON valid" << Qt::endl;
        return false;
    }

    /*
     *  Ensure the configuration file is an object
     */
    if (doc.isObject() == false)
    {
        qStdout() << "The configuration file is not a JSON Object" << Qt::endl;
        return false;
    }
    m_json = doc.object();

    return true;
}

void Config::show()
{
    qStdout() << "Configuration summary" << Qt::endl;
    qStdout() << "\tbroker : " << Qt::endl;
    qStdout() << "\t\taddress : " << getBrokerAdress() << Qt::endl;
    qStdout() << "\t\tport : " << getBrokerPort() <<Qt::endl;
    qStdout() << "\tlog : " << Qt::endl;
    qStdout() << "\t\tperiod_load : " << getPeriodLoad() << Qt::endl;
    qStdout() << "\t\tperiod_upload : " << getPeriodUpload() << Qt::endl;
    qStdout() << "\t\tlog_file_size_max : " << getSizeMaxLogFile() << Qt::endl;
    qStdout() << "\t\tfolder_load : " << getFolderLoad()  << Qt::endl;
    qStdout() << "\t\tfolder_upload : " << getFolderUpload()  << Qt::endl;
    qStdout() << "\t\tunits : " << Qt::endl;
    foreach (QString unit, getUnits())
    {
        qStdout() << "\t\t\t" << unit << Qt::endl;
    }

}

QString Config::getBrokerAdress()
{
    QJsonValue value = m_json["broker"];
    QJsonObject object = value.toObject();
    return object["address"].toString();
}

int Config::getBrokerPort()
{
    QJsonValue value = m_json["broker"];
    QJsonObject object = value.toObject();
    return object["port"].toInt();
}

int Config::getPeriodLoad()
{
    QJsonValue value = m_json["logs"];
    QJsonObject object = value.toObject();
    return object["period_load"].toInt();
}

int Config::getPeriodUpload()
{
    QJsonValue value = m_json["logs"];
    QJsonObject object = value.toObject();
    return object["period_upload"].toInt();
}

int Config::getSizeMaxLogFile()
{
    QJsonValue value = m_json["logs"];
    QJsonObject object = value.toObject();
    return object["log_file_size_max"].toInt();
}

QString Config::getFolderLoad()
{
    QJsonValue value = m_json["logs"];
    QJsonObject object = value.toObject();
    return object["folder_load"].toString();
}

QString Config::getFolderUpload()
{
    QJsonValue value = m_json["logs"];
    QJsonObject object = value.toObject();
    return object["folder_upload"].toString();
}

QStringList Config::getUnits()
{
    QStringList unitsList;
    QJsonValue log = m_json["logs"];
    QJsonObject logObject = log.toObject();
    QJsonArray unitsArray = logObject["units"].toArray();
    foreach (const QJsonValue & units, unitsArray)
    {
        unitsList << units.toString();
    }

    return unitsList;
}

