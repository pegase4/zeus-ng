#include <cstdlib>
#include "logservice.h"
#include "debug.h"


QMutex mutexLoadUpload;

LogService::LogService(QObject *parent) :
    QObject(parent)
{
}

LogService::LogService(QString service, int periodUpload, int periodLoad, QString dirUpload, QString dirload, int sizeMaxLogFile)
{
    m_service = service;
    m_periodUpload = periodUpload;
    m_periodLoad = periodLoad;
    m_dirUpload = dirUpload;
    m_dirLoad = dirload;
    m_sizeMaxLogFile = sizeMaxLogFile;
    m_cursor = "NONE";
    m_timerUpload = new QTimer();
    m_timerLoad = new QTimer();

    qStdout() << "folder load " << m_service << " : " << m_dirLoad << Qt::endl;
    qStdout() << "folder upload " << m_service << " : " << m_dirUpload << Qt::endl;
}

void LogService::start()
{
    QString path = m_dirLoad+m_service+".cursor";
    QFile file(path);
    if (file.open(QFile::ReadOnly))
    {
        qDebug() << "fichier ouvert "<< path;
        QTextStream flux(&file);
        m_cursor = flux.readAll();
        m_cursor.remove("\n");
        file.close();
    }
    else
    {
        qDebug() << "The configuration file " << path << " is not readable" << Qt::endl;
    }

    qStdout() << "Refresh file log for " << m_service << " on start" << Qt::endl;
    refresh();

    connect(m_timerLoad, SIGNAL(timeout()), SLOT(refresh()));
    m_timerLoad->start(m_periodLoad);
    qStdout() << "Launch timer m_timerLoad with period   " << m_periodLoad << "ms for " << m_service << Qt::endl;

    connect(m_timerUpload, SIGNAL(timeout()), SLOT(upload()));
    m_timerUpload->start(m_periodUpload);
    qStdout() << "Launch timer m_timerUpload with period " << m_periodUpload << "ms for " << m_service << Qt::endl;
}

void LogService::stop()
{
    m_timerUpload->stop();
    m_timerLoad->stop();

    // Mise à jour et préparation à l'upload des log
    qStdout() << "Refresh file log for " << m_service << " before stop" << Qt::endl;
    refresh();

    qStdout() << "Upload file log for " << m_service << " before stop" << Qt::endl;
    upload();
}

void LogService::upload()
{
    bool rc_renameFile;
    QDateTime dateTimeUpload;
    QString s_timeUpload = dateTimeUpload.currentDateTime().toString("yyyy-MM-dd_HH'h'mm'm'ss's'_000'ms'_000'us'_000'ns'");
    QString pathUpload = m_dirUpload+s_timeUpload+"_"+m_service+".log";
    QString pathLog = m_dirLoad+m_service+".log";
    QFile fileLog(pathLog);

    qStdout() << "Upload file log for " << m_service << Qt::endl;

    mutexLoadUpload.lock();
    
    qStdout() << "Upload : Get time OK. Upload log with date" << Qt::endl;

    rc_renameFile = fileLog.rename(pathUpload);
    if (rc_renameFile == false)
    {
        qDebug() << "move file from " << pathLog << " to " << pathUpload << " is correctly executed" << Qt::endl;
    }
    else
    {
        qDebug() << "move file from " << pathLog << " to " << pathUpload << " is not correctly executed" << Qt::endl;
    }

    mutexLoadUpload.unlock();
}

void LogService::refresh()
{
    QString exec;
    QString pathCursor = m_dirLoad+m_service+".cursor";
    QFile fileCursor(pathCursor);

    qStdout() << "Load (refresh) log for " << m_service << Qt::endl;

    if (fileCursor.open(QFile::ReadOnly))
    {
        QTextStream flux(&fileCursor);
        m_cursor = flux.readAll();
        m_cursor.remove("\n");
        fileCursor.close();
    }
    else
    {
        qDebug() << "The configuration file " << pathCursor << " is not readable" << Qt::endl;
    }
    if(m_cursor == "NONE")
    {
        if (m_service == "kernel")
        {
            exec = "journalctl --show-cursor -k > "+m_dirLoad+m_service+".log.tmp";
        }
        else
        {
            exec = "journalctl --show-cursor -u "+m_service+" > "+m_dirLoad+m_service+".log.tmp";
        }
        system(exec.toStdString().c_str());

        qStdout() << "File generation " << m_dirLoad+m_service << ".log" << Qt::endl;
    }
    else
    {
        if (m_service == "kernel")
        {
            exec = "journalctl -c \""+m_cursor+"\" --show-cursor -k > "+m_dirLoad+m_service+".log.tmp";;
        }
        else
        {
            exec = "journalctl -c \""+m_cursor+"\" --show-cursor -u "+m_service+" > "+m_dirLoad+m_service+".log.tmp";
        }
        system(exec.toStdString().c_str());

        qStdout() << "File refresh " << m_dirLoad+m_service << ".log" << Qt::endl;
    }

    QString path = m_dirLoad+m_service+".log.tmp";
    QFile fileTmp(path);
    if (fileTmp.open(QFile::ReadOnly))
    {
        QTextStream flux(&fileTmp);
        QString log = flux.readAll();
        fileTmp.close();
        QStringList logCursor = log.split("-- cursor: ");
        QString cursorTemp;
        if(logCursor.size() == 2)
        {
            cursorTemp = logCursor.at(1);
            cursorTemp.remove("\n");
        }

        if(logCursor.size() == 2 && cursorTemp != m_cursor)
        {
            qStdout() << "cursor change for log of " << m_service << Qt::endl;
            qStdout() << m_cursor << " => " << logCursor.at(1) << Qt::endl;

            writeLog(logCursor.at(0));
            
            changeCursor(logCursor.at(1));
        }
    }
    else
    {
        qDebug() << "The configuration file " << path << " is not readable" << Qt::endl;
    }

    // Suppression du fichier temporaire de log
    QFile fileToRemove;
    fileToRemove.remove(m_dirLoad+m_service+".log.tmp");
    
    // Vérification de la taille du fichier de log pour envoi sur le servur s'il dépasse la taille maximale autorisée
    path = m_dirLoad+m_service+".log";
    QFile file(path);
    
    if (file.open(QFile::ReadOnly))
    {
        if (file.size() > m_sizeMaxLogFile)
        {
            file.close();
            upload();
            qStdout() << "Upload file due to size limit log file reach for " << m_service << Qt::endl;
        }
        else
        {
            file.close();
        }
   }    
}


void LogService::changeCursor(QString cursor)
{
    QString path = m_dirLoad+m_service+".cursor";
    QFile file(path);
    if (file.open(QFile::WriteOnly | QIODevice::Text))
    {
        QTextStream flux(&file);
        flux.setCodec("UTF-8");
        flux << cursor;
        file.close();
    }
    else
    {
        qDebug() << "The configuration file " << path << " is not readable" << Qt::endl;
    }
}


void LogService::writeLog(QString log)
{
    QString path = m_dirLoad+m_service+".log";
    QFile file(path);
    QStringList listStringTemp = log.split("\n");

    mutexLoadUpload.lock();

    if (file.open(QFile::WriteOnly | QIODevice::Append))
    {
        QTextStream flux(&file);
        flux.setCodec("UTF-8");
        for(int i=2 ; i<listStringTemp.size() ; i++)
        {
            flux << listStringTemp.at(i);
            if (i < listStringTemp.size()-1)
            {
                flux << "\n";
            }
        }

        file.close();
    }
    else
    {
        qDebug() << "The configuration file " << path << " is not readable" << Qt::endl;
    }

    mutexLoadUpload.unlock();
}
